package com.flycms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author kaifei sun
 */
@SpringBootApplication(scanBasePackages = {"com.flycms"},exclude = { DataSourceAutoConfiguration.class })
public class Application
{
    public static void main(String[] args) {
        System.setProperty("spring.devtools.restart.enabled", "false");
        System.setProperty("tomcat.util.http.parser.HttpParser.requestTargetAllow","|{}");
        System.setProperty("es.set.netty.runtime.available.processors","false");
        SpringApplication.run(Application.class, args);
    }
}
