package com.flycms.modules.notify.controller.manage;

import com.flycms.modules.notify.service.ISmsCodeService;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.notify.domain.SmsCode;
import com.flycms.modules.notify.domain.dto.SmsCodeDto;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

/**
 * 短信验证码Controller
 * 
 * @author kaifei sun
 * @date 2020-05-27
 */
@RestController
@RequestMapping("/system/notify/SmsCode")
public class SmsCodeAdminController extends BaseController
{
    @Autowired
    private ISmsCodeService smsCodeService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增短信验证码
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsCode:add')")
    @Log(title = "短信验证码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SmsCode smsCode)
    {
        return toAjax(smsCodeService.insertSmsCode(smsCode));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除短信验证码
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsCode:remove')")
    @Log(title = "短信验证码", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(smsCodeService.deleteSmsCodeByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信验证码
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsCode:edit')")
    @Log(title = "短信验证码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SmsCode smsCode)
    {
        return toAjax(smsCodeService.updateSmsCode(smsCode));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询短信验证码列表
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsCode:list')")
    @GetMapping("/list")
    public TableDataInfo list(SmsCode smsCode,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "create_time") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SmsCodeDto> pager = smsCodeService.selectSmsCodePager(smsCode, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出短信验证码列表
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsCode:export')")
    @Log(title = "短信验证码", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SmsCode smsCode,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @Sort @RequestParam(defaultValue = "id") String sort,
            @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SmsCodeDto> pager = smsCodeService.selectSmsCodePager(smsCode, pageNum, pageSize, sort, order);
        ExcelUtil<SmsCodeDto> util = new ExcelUtil<SmsCodeDto>(SmsCodeDto.class);
        return util.exportExcel(pager.getList(), "SmsCode");
    }

    /**
     * 获取短信验证码详细信息
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsCode:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(smsCodeService.selectSmsCodeById(id));
    }
}
