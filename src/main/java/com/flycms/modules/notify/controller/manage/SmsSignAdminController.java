package com.flycms.modules.notify.controller.manage;

import com.flycms.modules.notify.service.ISmsSignService;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.notify.domain.SmsSign;
import com.flycms.modules.notify.domain.dto.SmsSignDto;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

/**
 * 短信签名Controller
 * 
 * @author admin
 * @date 2020-05-27
 */
@RestController
@RequestMapping("/system/notify/SmsSign")
public class SmsSignAdminController extends BaseController
{
    @Autowired
    private ISmsSignService smsSignService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增短信签名
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsSign:add')")
    @Log(title = "短信签名", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SmsSign smsSign)
    {
        return toAjax(smsSignService.insertSmsSign(smsSign));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除短信签名
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsSign:remove')")
    @Log(title = "短信签名", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(smsSignService.deleteSmsSignByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信签名
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsSign:edit')")
    @Log(title = "短信签名", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SmsSign smsSign)
    {
        return toAjax(smsSignService.updateSmsSign(smsSign));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询短信签名列表
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsSign:list')")
    @GetMapping("/list")
    public TableDataInfo list(SmsSign smsSign,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "create_time") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SmsSignDto> pager = smsSignService.selectSmsSignPager(smsSign, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出短信签名列表
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsSign:export')")
    @Log(title = "短信签名", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SmsSign smsSign,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @Sort @RequestParam(defaultValue = "id") String sort,
            @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SmsSignDto> pager = smsSignService.selectSmsSignPager(smsSign, pageNum, pageSize, sort, order);
        ExcelUtil<SmsSignDto> util = new ExcelUtil<SmsSignDto>(SmsSignDto.class);
        return util.exportExcel(pager.getList(), "SmsSign");
    }

    /**
     * 获取短信签名详细信息
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsSign:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(smsSignService.selectSmsSignById(id));
    }
}
