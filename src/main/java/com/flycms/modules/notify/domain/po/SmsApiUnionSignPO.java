package com.flycms.modules.notify.domain.po;

import lombok.Data;

/**
 * 短信接口对象 fly_sms_api
 * 
 * @author admin
 * @date 2020-05-27
 */
@Data
public class SmsApiUnionSignPO
{
    private static final long serialVersionUID = 1L;
    /** KeyId */
    private String accessKeyId;
    /** KeySecret */
    private String accessKeySecret;
    //签名名称
    private String signName;
    //模板编码
    private String  templateCode;
}
