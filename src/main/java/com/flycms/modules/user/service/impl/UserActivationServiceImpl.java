package com.flycms.modules.user.service.impl;


import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.user.mapper.UserActivationMapper;
import com.flycms.modules.user.domain.UserActivation;
import com.flycms.modules.user.domain.dto.UserActivationDTO;
import com.flycms.modules.user.service.IUserActivationService;

import java.util.ArrayList;
import java.util.List;

/**
 * 注册码Service业务层处理
 * 
 * @author admin
 * @date 2020-12-08
 */
@Service
public class UserActivationServiceImpl implements IUserActivationService 
{
    @Autowired
    private UserActivationMapper userActivationMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增注册码
     *
     * @param userActivation 注册码
     * @return 结果
     */
    @Override
    public int insertUserActivation(UserActivation userActivation)
    {
        userActivation.setId(SnowFlakeUtils.nextId());
        userActivation.setReferTime(DateUtils.getNowDate());
        return userActivationMapper.insertUserActivation(userActivation);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除注册码
     *
     * @param ids 需要删除的注册码ID
     * @return 结果
     */
    @Override
    public int deleteUserActivationByIds(Long[] ids)
    {
        return userActivationMapper.deleteUserActivationByIds(ids);
    }

    /**
     * 删除注册码信息
     *
     * @param id 注册码ID
     * @return 结果
     */
    @Override
    public int deleteUserActivationById(Long id)
    {
        return userActivationMapper.deleteUserActivationById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改注册码
     *
     * @param userActivation 注册码
     * @return 结果
     */
    @Override
    public int updateUserActivation(UserActivation userActivation)
    {
        return userActivationMapper.updateUserActivation(userActivation);
    }

    /**
     * 按用户名（邮箱、手机号）+ 验证码查询修改验证状态为已验证，0未验证，1为已验证
     *
     * @param userName 用户名
     * @param code 验证码
     * @return 结果
     */
    @Override
    public int updateUserActivationByStatus(String userName,String code)
    {
        return userActivationMapper.updateUserActivationByStatus(userName,code);
    }

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询验证码在当前时间5分钟内获取并且是否过时或不存在
     *
     * @param userName
     *         查询的用户名
     * @param codeType
     *         查询的验证码类型，1手机注册验证码,2安全手机设置验证码,3密码重置验证码
     * @param code
     *         验证码
     * @return
     */
    @Override
    public boolean checkUserActivationCode(String userName,Integer codeType,String code){
        UserActivation activation = userActivationMapper.findByUserActivationCode(userName,codeType);
        if(activation!=null){
            if(activation.getCode().equals(code)){
                return true;
            }
        }
        return false;
    }

    /**
     * 查询注册码
     * 
     * @param id 注册码ID
     * @return 注册码
     */
    @Override
    public UserActivationDTO findUserActivationById(Long id)
    {
        UserActivation userActivation=userActivationMapper.findUserActivationById(id);
        return BeanConvertor.convertBean(userActivation,UserActivationDTO.class);
    }


    /**
     * 查询注册码列表
     *
     * @param userActivation 注册码
     * @return 注册码
     */
    @Override
    public Pager<UserActivationDTO> selectUserActivationPager(UserActivation userActivation, Integer page, Integer limit, String sort, String order)
    {
        Pager<UserActivationDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(userActivation);

        List<UserActivation> userActivationList=userActivationMapper.selectUserActivationPager(pager);
        List<UserActivationDTO> dtolsit = new ArrayList<UserActivationDTO>();
        userActivationList.forEach(entity -> {
            UserActivationDTO dto = new UserActivationDTO();
            dto.setId(entity.getId());
            dto.setUserName(entity.getUserName());
            dto.setCode(entity.getCode());
            dto.setCodeType(entity.getCodeType());
            dto.setReferTime(entity.getReferTime());
            dto.setReferStatus(entity.getReferStatus());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(userActivationMapper.queryUserActivationTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的注册码列表
     *
     * @param userActivation 注册码
     * @return 注册码集合
     */
    @Override
    public List<UserActivationDTO> exportUserActivationList(UserActivation userActivation) {
        return BeanConvertor.copyList(userActivationMapper.exportUserActivationList(userActivation),UserActivationDTO.class);
    }
}
