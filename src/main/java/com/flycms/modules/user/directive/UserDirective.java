package com.flycms.modules.user.directive;

import com.flycms.common.utils.SessionUtils;
import com.flycms.framework.web.tag.BaseTag;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.domain.vo.UserInfoVO;
import com.flycms.modules.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class UserDirective extends BaseTag {

    @Autowired
    private IUserService userService;

    public UserDirective() {
        super(UserDirective.class.getName());
    }

    //用户查询列表
    public Object paginate(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        String nickname = getParam(params, "nickname");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        User user = new User();
        user.setNickname(nickname);
        return userService.selectUserPager(user, pageNum, pageSize, sort, order);
    }

    //登录状态
    public Object userLogin(Map params) {
        User user = SessionUtils.getUser();
        return user;
    }

    //用户信息
    public Object user(Map params) {
        Long userId = getLongParam(params, "id");
        UserInfoVO user = null;
        if(userId != null){
            user = userService.findUserInfoById(userId);
        }
        return user;
    }
}
