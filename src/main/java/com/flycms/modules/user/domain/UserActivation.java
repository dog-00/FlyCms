package com.flycms.modules.user.domain;

import java.util.Date;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 注册码对象 fly_user_activation
 * 
 * @author admin
 * @date 2020-12-08
 */
@Data
public class UserActivation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 账号 */
    private String userName;
    /** 验证码 */
    private String code;
    /** 类型 */
    private Integer codeType;
    /** 时间 */
    private Date referTime;
    /** 状态 */
    private Integer referStatus;
}
