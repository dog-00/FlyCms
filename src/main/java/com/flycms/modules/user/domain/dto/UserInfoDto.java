package com.flycms.modules.user.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户信息
 * 
 * @author CHENBO
 * @since 1.0.0
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class UserInfoDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonSerialize(using = ToStringSerializer.class)
	private Long userId;
	private String nickName;
	private String avatarUrl;
	private String country;
	private String province;
	private String city;
	private String language;
	private Byte gender;
	private String mobile;
	private Byte userLevel;// 用户层级 0 普通用户，1 VIP用户，2 区域代理用户
	private String userLevelDesc;// 代理用户描述
	
	private Byte status;//状态
	private String registerDate;//注册日期
}
