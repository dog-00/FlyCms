package com.flycms.modules.user.domain;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 用户用小程序注册信息对象 fly_user_app
 * 
 * @author admin
 * @date 2020-05-30
 */
@Data
public class UserApp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** openid */
    private String openId;
    /** unionid */
    private String unionId;
    /** 平台 */
    private String platform;
    /** 头像 */
    private String avatarUrl;
    /** 昵称 */
    private String nickName;
    /** 性别 */
    private Integer gender;
    /** 所在国家 */
    private String country;
    /** 所在省份 */
    private String province;
    /** 所在城市 */
    private String city;
    /** 使用的语言 */
    private String language;
}
