package com.flycms.modules.user.domain.dto;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户数据传输对象 fly_user
 * 
 * @author kaifei sun
 * @date 2020-10-30
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class UserDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 账号 */
    @Excel(name = "账号")
    private String username;
    /** 手机号码 */
    @Excel(name = "手机号码")
    private String mobile;
    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;
    /** 性别 */
    @Excel(name = "性别")
    private Integer gender;
    /** 生日 */
    @Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;
    /** 登录时间 */
    @Excel(name = "登录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastLoginTime;
    /** 登录IP */
    @Excel(name = "登录IP")
    private String lastLoginIp;
    /** 用户层级 0 普通用户，1 VIP用户，2 区域代理用户 */
    @Excel(name = "用户层级 0 普通用户，1 VIP用户，2 区域代理用户")
    private Integer userLevel;
    /** 用户昵称或网络名称 */
    @Excel(name = "昵称")
    private String nickname;
    /** 用户头像图片 */
    @Excel(name = "头像")
    private String avatar;
    /** 0 可用, 1 禁用, 2 注销 */
    @Excel(name = "审核")
    private Integer status;

}
