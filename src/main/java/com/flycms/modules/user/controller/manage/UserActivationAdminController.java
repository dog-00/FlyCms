package com.flycms.modules.user.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.user.domain.UserActivation;
import com.flycms.modules.user.domain.dto.UserActivationDTO;
import com.flycms.modules.user.service.IUserActivationService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 注册码Controller
 * 
 * @author admin
 * @date 2020-12-08
 */
@RestController
@RequestMapping("/system/user/activation")
public class UserActivationAdminController extends BaseController
{
    @Autowired
    private IUserActivationService userActivationService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增注册码
     */
    @PreAuthorize("@ss.hasPermi('user:activation:add')")
    @Log(title = "注册码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserActivation userActivation)
    {
        return toAjax(userActivationService.insertUserActivation(userActivation));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除注册码
     */
    @PreAuthorize("@ss.hasPermi('user:activation:remove')")
    @Log(title = "注册码", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(userActivationService.deleteUserActivationByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改注册码
     */
    @PreAuthorize("@ss.hasPermi('user:activation:edit')")
    @Log(title = "注册码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserActivation userActivation)
    {
        return toAjax(userActivationService.updateUserActivation(userActivation));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询注册码列表
     */
    @PreAuthorize("@ss.hasPermi('user:activation:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserActivation userActivation,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<UserActivationDTO> pager = userActivationService.selectUserActivationPager(userActivation, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出注册码列表
     */
    @PreAuthorize("@ss.hasPermi('user:activation:export')")
    @Log(title = "注册码", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserActivation userActivation)
    {
        List<UserActivationDTO> userActivationList = userActivationService.exportUserActivationList(userActivation);
        ExcelUtil<UserActivationDTO> util = new ExcelUtil<UserActivationDTO>(UserActivationDTO.class);
        return util.exportExcel(userActivationList, "activation");
    }

    /**
     * 获取注册码详细信息
     */
    @PreAuthorize("@ss.hasPermi('user:activation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(userActivationService.findUserActivationById(id));
    }

}
