package com.flycms.modules.user.controller.front;

import com.flycms.common.utils.SecurityUtils;
import com.flycms.common.utils.SessionUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.notify.service.IEmailService;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.domain.UserActivation;
import com.flycms.modules.user.domain.vo.UserInfoVO;
import com.flycms.modules.user.service.IUserActivationService;
import com.flycms.modules.user.service.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * 用户后台Controller
 * 
 * @author admin
 * @date 2020-07-08
 */
@Controller
public class UserController extends BaseController
{
    @Autowired
    private IUserService userService;

    @Autowired
    private IEmailService emailService;

    @Autowired
    private IUserActivationService userActivationService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 邮箱注册
     *
     * @return
     */
    @GetMapping(value = "/register/email")
    public String regEmail(ModelMap modelMap){
        return theme.getPcTemplate("/user/register_email");
    }

    /**
     * 邮箱获取验证码
     *
     * @param username
     * @param captcha  备用图形验证
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/register/email")
    public AjaxResult regEmailCode(@RequestParam(value = "username", required = false) String username,
                                   @RequestParam(value = "captcha", required = false) String captcha){
        if(!StrUtils.isEmail(username)){
            return AjaxResult.error("邮箱格式错误");
        }
        User entity = userService.findUserByEmail(username);
        if(entity!=null){
            return AjaxResult.error("该邮箱已被占用！");
        }
        String newCode=StrUtils.getUserCaptcha().toLowerCase();
        emailService.sendEmail(username,newCode,"reg_email");
        UserActivation userActivation=new UserActivation();
        userActivation.setUserName(username);
        userActivation.setCode(newCode);
        userActivation.setCodeType(1);
        userActivation.setReferStatus(0);
        userActivationService.insertUserActivation(userActivation);
        return AjaxResult.success("验证码已发送到您邮箱");
    }

    /**
     * 增加邮箱注册用户
     */
    @ResponseBody
    @PostMapping(value = "/register/add/email")
    public AjaxResult addEmailUser(@RequestParam(value = "username", required = false) String username,
                                   @RequestParam(value = "code", required = false) String code,
                                   @RequestParam(value = "password", required = false) String password,
                                    @RequestParam(value = "password2", required = false) String password2,
                                    @RequestParam(value = "invite", required = false) String invite,
                                    @RequestParam(value = "captcha", required = false) String captcha)
    {
        username=username.trim();
        if(StrUtils.isEmpty(username)){
            return AjaxResult.error("用户名不能为空");
        }
        if(!StrUtils.isEmail(username)){
            return AjaxResult.error("邮箱格式错误");
        }
        password=password.trim();
        if(StrUtils.isEmpty(password)){
            return AjaxResult.error("密码不能为空");
        }
        password2=password2.trim();
        if(!password.equals(password2)){
            return AjaxResult.error("两次密码不一样");
        }

        if(!StrUtils.isEmpty(captcha)){
            captcha=captcha.trim();
            String kaptcha = (String) session.getAttribute("captcha");
            if(!captcha.equals(kaptcha)){
                return AjaxResult.error("验证码错误");
            }
        }
        User entity = userService.findUserByEmail(username);
        if(entity!=null){
            return AjaxResult.error("该邮箱已被占用！");
        }
        if(userActivationService.checkUserActivationCode(username,1,code)){
            return AjaxResult.error("验证码错误或者已失效！");
        }
        //修改验证码状态
        userActivationService.updateUserActivationByStatus(username,code);
        User user= new User();
        user.setEmail(username);
        user.setPassword(SecurityUtils.encryptPassword(password));
        int conut=userService.insertUser(user);
        if(conut>0){
            SessionUtils.setUser(user);
        }
        return AjaxResult.success("注册成功","/user/my/");
    }

    /**
     * 电话注册
     *
     * @return
     */
    @GetMapping(value = "/register/phone")
    public String regPhone(ModelMap modelMap){
        return theme.getPcTemplate("/user/register_phone");
    }

    /**
     * 新增手机号码注册用户
     */
    @PreAuthorize("@ss.hasPermi('user:user:add')")
    @Log(title = "用户管理", businessType = BusinessType.INSERT)
    @PostMapping(value = "/register/add/phone")
    public AjaxResult add(@RequestParam(value = "username", required = false) String username,
                          @RequestParam(value = "password", required = false) String password,
                          @RequestParam(value = "password2", required = false) String password2,
                          @RequestParam(value = "invite", required = false) String invite,
                          @RequestParam(value = "captcha", required = false) String captcha)
    {
        username=username.trim();
        if(StrUtils.isEmpty(username)){
            return AjaxResult.error("用户名不能为空");
        }
        password=password.trim();
        if(StrUtils.isEmpty(password)){
            return AjaxResult.error("密码不能为空");
        }
        password2=password2.trim();
        if(!password.equals(password2)){
            return AjaxResult.error("两次密码不一样");
        }
        captcha=captcha.trim();
        if(!StrUtils.isEmpty(captcha)){
            String kaptcha = (String) session.getAttribute("captcha");
            if(!captcha.equals(kaptcha)){
                return AjaxResult.error("验证码错误");
            }
        }
        User user= new User();
        user.setMobile(username);
        user.setPassword(SecurityUtils.encryptPassword(password));
        int conut=userService.insertUser(user);
        if(conut>0){
            SessionUtils.setUser(user);
        }
        return AjaxResult.success("注册成功","/user/my/");
    }

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 用户登录
     *
     * @return
     */
    @GetMapping(value = {"/user/login/" , "/user/login"})
    public String userLogin(@RequestParam(value = "redirectUrl",required = false) String redirectUrl,ModelMap modelMap){
        User user = SessionUtils.getUser();
        if(user != null){
            return redirect("/");
        }
        modelMap.addAttribute("redirectUrl",redirectUrl);
        return theme.getPcTemplate("user/login");
    }

    //登录处理
    @ResponseBody
    @PostMapping(value = "/user/login")
    public AjaxResult userLogin(
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "rememberMe", required = false) String rememberMe,
            @RequestParam(value = "redirectUrl",required = false) String redirectUrl) {
        try {
            if (StringUtils.isBlank(username)) {
                return AjaxResult.error("用户名不能为空");
            }
            if (StringUtils.isBlank(password)) {
                return AjaxResult.error("密码不能为空");
            } else if (password.length() < 6 && password.length() > 30) {
                return AjaxResult.error("密码最少6个字符，最多30个字符");
            }

            boolean keepLogin = "1".equals(rememberMe) ? true : false;
            User entity = null;
            if(StrUtils.checkUserName(username)){
                entity = userService.findUserByUsername(username);
            }else if(StrUtils.checkPhoneNumber(username)){
                entity = userService.findUserByMobile(username);
            }else if(StrUtils.checkEmail(username)){
                entity = userService.findUserByEmail(username);
            }
            if(entity==null){
                return AjaxResult.error("帐号不存在");
            }else{
                if(!BCrypt.checkpw(password.trim().toString(), entity.getPassword())){
                    return AjaxResult.error("密码错误");
                }
                SessionUtils.setUser(entity);
                if (!StrUtils.isEmpty(redirectUrl)){
                    return AjaxResult.success("操作成功", redirectUrl);
                }
                return AjaxResult.success("操作成功", "/user/my/");
            }
        } catch (Exception e) {
            return AjaxResult.error("帐号或密码错误。");
        }
    }

    // 登出
    @GetMapping("/user/logout")
    public String logout(HttpSession session) {
        SessionUtils.removeUser();
        return redirect("/");
    }

    /**
     * 用户首页
     *
     * @return
     */
    @GetMapping(value = {"/user/my/" , "/user/my"})
    public String userIndex(ModelMap modelMap){
        User user = SessionUtils.getUser();
        UserInfoVO info = userService.findUserInfoById(user.getId());
        modelMap.addAttribute("user", info);
        return theme.getPcTemplate("user/my_index");
    }

    /**
     * 用户我加入的小组
     *
     * @return
     */
    @GetMapping(value = {"/user/my/group/" , "/user/my/group"})
    public String userGroup(ModelMap modelMap){
        User user = SessionUtils.getUser();
        modelMap.addAttribute("user", user);
        return theme.getPcTemplate("user/my_group");
    }

    /**
     * 我的设置
     *
     * @return
     */
    @GetMapping(value = {"/user/my/setting/" , "/user/my/setting"})
    public String userSetting(ModelMap modelMap){
        User user = SessionUtils.getUser();
        modelMap.addAttribute("user", user);
        return theme.getPcTemplate("user/my_setting");
    }

    /**
     * 我的支付
     *
     * @return
     */
    @GetMapping(value = {"/user/my/pay/" , "/user/my/pay"})
    public String userPay(ModelMap modelMap){
        User user = SessionUtils.getUser();
        modelMap.addAttribute("user", user);
        return theme.getPcTemplate("user/my_pay");
    }
}
