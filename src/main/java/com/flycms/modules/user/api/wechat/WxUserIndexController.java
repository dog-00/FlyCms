package com.flycms.modules.user.api.wechat;

import com.flycms.modules.user.annotation.LoginUser;
import com.flycms.modules.user.util.ResponseUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api("用户注册登录")
@RestController
@RequestMapping("/wx/user")
@Validated
public class WxUserIndexController {
    private static final Logger logger = LoggerFactory.getLogger(WxAuthController.class);
    @GetMapping("/index")
    public Object index(@LoginUser String userId) {
        logger.info("【请求开始】访问首页,请求参数,userId:{}" , userId);
        return ResponseUtil.ok();
    }
}
