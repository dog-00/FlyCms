package com.flycms.modules.group.controller.front;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.SessionUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.dto.GroupTopicDTO;
import com.flycms.modules.group.domain.dto.GroupUserDTO;
import com.flycms.modules.group.domain.vo.GroupTopicVO;
import com.flycms.modules.group.service.IGroupService;
import com.flycms.modules.group.service.IGroupTopicService;
import com.flycms.modules.group.service.IGroupUserService;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 官网设置Controller
 * 
 * @author admin
 * @date 2020-07-08
 */
@Controller
public class GroupTopicController extends BaseController
{
    @Autowired
    private IGroupTopicService groupTopicService;

    @Autowired
    private IGroupService groupService;

    @Autowired
    private IGroupUserService groupUserService;

    @Autowired
    private IUserService userService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 添加话题
     *
     * @return
     */
    @GetMapping("/user/my/topic/{groupId}/create")
    public String create(@PathVariable("groupId") String groupId,ModelMap modelMap){
        if (!StrUtils.checkLong(groupId)) {
            return forward("/error/404");
        }
        Group group= groupService.findGroupById(Long.valueOf(groupId));
        if(group == null){
            return forward("/error/404");
        }
        User user = SessionUtils.getUser();
        if(user == null){
            modelMap.addAttribute("message","用户未登录");
            return theme.getPcTemplate("/error/error_tip");
        }
        GroupUserDTO userDTO=groupUserService.findGroupUserById(user.getId(),group.getId());
        if(userDTO == null || userDTO.getStatus() == 0){
            modelMap.addAttribute("message","用户为加入或者加入申请未审核");
            return theme.getPcTemplate("/error/error_tip");
        }
        modelMap.addAttribute("group",group);
        modelMap.addAttribute("groupId",groupId);
        return theme.getPcTemplate("/topic/create");
    }

    /**
     * 新增群组(小组)
     */
    @PostMapping("/user/my/topic/create")
    @ResponseBody
    public AjaxResult add(GroupTopicVO topicVo) throws Exception
    {
        if (!StrUtils.checkLong(topicVo.getGroupId())) {
            return AjaxResult.error("未选择加入发布的小组");
        }
        Group group= groupService.findGroupById(Long.valueOf(topicVo.getGroupId()));
        if(group == null){
            return AjaxResult.error("小组不存在");
        }
        if(group.getIspost() == 0){
            return AjaxResult.error("本小组暂不允许发帖！");
        }
        User user = SessionUtils.getUser();
        if(user == null){
            return AjaxResult.error("未登录或者用户不存在");
        }
        GroupUserDTO userDTO=groupUserService.findGroupUserById(user.getId(),group.getId());
        if(userDTO == null || userDTO.getStatus() == 0){
            return AjaxResult.error("您未关注本小组或者未通过审核");
        }
        GroupTopic groupTopic=new GroupTopic();
        groupTopic.setGroupId(group.getId());
        groupTopic.setTitle(topicVo.getTitle());
        groupTopic.setContent(topicVo.getContent());
        groupTopic.setColumnId(Long.valueOf(topicVo.getColumnId()));
        groupTopic.setIscomment(topicVo.getIscomment());
        groupTopic.setIscommentshow(topicVo.getIscommentshow());
        groupTopic.setScore(topicVo.getScore());
        //是否发帖审核  0不允许，1允许
        if(group.getIspostaudit() ==1){
            groupTopic.setStatus(0);
        }else{
            groupTopic.setStatus(1);
        }
        if (UserConstants.NOT_UNIQUE.equals(groupTopicService.checkGroupTopicNameUnique(groupTopic)))
        {
            return AjaxResult.error("发布标题'" + groupTopic.getTitle() + "'失败，请勿重复发布！");
        }
        return toAjax(groupTopicService.insertGroupTopic(groupTopic));
    }
    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除用户账户
     */
    @PostMapping("/user/my/topic/delete")
    @ResponseBody
    public AjaxResult remove(@RequestParam("id") String id)
    {
        if (!StrUtils.checkLong(id)) {
            return AjaxResult.error("话题id错误");
        }
        GroupTopicDTO topic= groupTopicService.findGroupTopicById(Long.valueOf(id));
        if(topic == null){
            return AjaxResult.error("话题不存在");
        }
        User user = SessionUtils.getUser();
        if(user == null){
            return AjaxResult.error("未登录或者用户不存在");
        }
        if(!user.getId().equals(topic.getUserId())){
            return AjaxResult.error("不能删除不属于您的文章！");
        }
        int count = groupTopicService.updateDeleteGroupTopicById(Long.valueOf(id));
        if(count == 0){
            return AjaxResult.error("删除话题失败！");
        }
        groupService.updateCountTopic(topic.getGroupId());
        groupTopicService.updateUserCountTopic(topic.getUserId());
        return AjaxResult.success("话题删除成功","/group/"+topic.getGroupId());
    }

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 添加话题
     *
     * @return
     */
    @GetMapping("/user/my/topic/{topicId}/update/")
    public String updateGroupTopic(@PathVariable("topicId") String topicId,ModelMap modelMap){
        if (!StrUtils.checkLong(topicId)) {
            return forward("/error/404");
        }
        User user = SessionUtils.getUser();
        if(user == null){
            modelMap.addAttribute("message","用户未登录");
            return theme.getPcTemplate("/error/error_tip");
        }

        GroupTopicDTO topic= groupTopicService.findGroupTopicById(Long.valueOf(topicId));
        if(topic == null){
            return forward("/error/404");
        }
        if(!user.getId().equals(topic.getUserId())){
            modelMap.addAttribute("message","不能修改不属于您的文章！");
            return theme.getPcTemplate("/error/error_tip");
        }
        modelMap.addAttribute("topic",topic);
        return theme.getPcTemplate("/topic/create");
    }


    /**
     * 修改小组话题
     */
    /**
     * 新增群组(小组)
     */
    @PostMapping("/user/my/topic/update")
    @ResponseBody
    public AjaxResult updateGroupTopic(GroupTopicVO topicVo) throws Exception
    {
        if (!StrUtils.checkLong(topicVo.getId())) {
            return AjaxResult.error("话题id错误");
        }
        GroupTopicDTO topic= groupTopicService.findGroupTopicById(Long.valueOf(topicVo.getId()));
        if(topic == null){
            return AjaxResult.error("话题不存在");
        }
        User user = SessionUtils.getUser();
        if(user == null){
            return AjaxResult.error("未登录或者用户不存在");
        }
        if(!user.getId().equals(topic.getUserId())){
            return AjaxResult.error("不能修改不属于您的文章！");
        }

        Group group= groupService.findGroupById(topic.getGroupId());
        if(group == null){
            return AjaxResult.error("小组不存在");
        }
        GroupTopic groupTopic=new GroupTopic();
        groupTopic.setId(Long.valueOf(topicVo.getId()));
        groupTopic.setTitle(topicVo.getTitle());
        groupTopic.setContent(topicVo.getContent());
        groupTopic.setColumnId(Long.valueOf(topicVo.getColumnId()));
        groupTopic.setIscomment(topicVo.getIscomment());
        groupTopic.setIscommentshow(topicVo.getIscommentshow());
        groupTopic.setScore(topicVo.getScore());
        //是否发帖审核  0不允许，1允许
        if(group.getIspostaudit() ==1){
            groupTopic.setStatus(0);
        }else{
            groupTopic.setStatus(1);
        }
        if (UserConstants.NOT_UNIQUE.equals(groupTopicService.checkGroupTopicNameUnique(groupTopic)))
        {
            return AjaxResult.error("发布标题'" + groupTopic.getTitle() + "'失败，请勿重复发布！");
        }
        return toAjax(groupTopicService.updateGroupTopic(groupTopic));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 详细内容
     *
     * @return
     */
    @GetMapping(value = {"/topic/{id}/" , "/topic/{id}"})
    public String detail(@PathVariable("id") String id, ModelMap modelMap){
        if (!StrUtils.checkLong(id)) {
            return forward("/error/404");
        }
        GroupTopicDTO content=groupTopicService.findGroupTopicById(Long.valueOf(id));
        if(content == null){
            return forward("/error/404");
        }
        if(content.getIsaudit() != 1){
            modelMap.addAttribute("message","该内容小组管理员未审核");
            return theme.getPcTemplate("/error/error_tip");
        }
        if(content.getStatus() != 1){
            modelMap.addAttribute("message","该内容管理员未审核或者未审核通过，请联系管理员");
            return theme.getPcTemplate("/error/error_tip");
        }
        modelMap.addAttribute("content",content);
        return theme.getPcTemplate("/topic/detail");
    }


}
