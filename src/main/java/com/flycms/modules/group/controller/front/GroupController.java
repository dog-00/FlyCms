package com.flycms.modules.group.controller.front;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.SessionUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.GroupUser;
import com.flycms.modules.group.domain.GroupUserIsaudit;
import com.flycms.modules.group.domain.dto.GroupDTO;
import com.flycms.modules.group.domain.dto.GroupUserDTO;
import com.flycms.modules.group.service.IGroupService;
import com.flycms.modules.group.service.IGroupUserIsauditService;
import com.flycms.modules.group.service.IGroupUserService;
import com.flycms.modules.user.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.beans.Transient;

/**
 * 官网设置Controller
 * 
 * @author admin
 * @date 2020-07-08
 */
@Controller
public class GroupController extends BaseController
{
    private static final Logger log = LoggerFactory.getLogger(GroupController.class);

    @Autowired
    private IGroupService groupService;

    @Autowired
    private IGroupUserService groupUserService;

    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增群组(小组)
     */
    @GetMapping(value = {"/user/my/group/create/" , "/user/my/group/create"})
    public String add(Group group,ModelMap modelMap)
    {
        User user = SessionUtils.getUser();
        if(user == null){
            return forward("/user/login");
        }
        return theme.getPcTemplate("/group/create");
    }

    /**
     * 新增群组(小组)
     */
    @PostMapping("/user/my/group/create")
    @ResponseBody
    public AjaxResult add(Group group) throws Exception
    {
        if (UserConstants.NOT_UNIQUE.equals(groupService.checkGroupGroupNameUnique(group)))
        {
            return AjaxResult.error("新增群组(小组)'" + group.getGroupName() + "'失败，群组名称已存在");
        }
        return toAjax(groupService.insertGroup(group));
    }

    /**
     * 新增群组和用户对应关系
     */
    @PostMapping("/group/user/follow")
    @ResponseBody
    public AjaxResult add(GroupUser groupUser)
    {
        User user = SessionUtils.getUser();
        if(user == null){
            return AjaxResult.error(101,"请登录后关注");
        }
        return groupUserService.insertGroupUser(groupUser);
    }

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 首页
     *
     * @return
     */
    @GetMapping(value = {"/group/" , "/group/index"})
    public String groupList(@RequestParam(value = "p", defaultValue = "1") int p,ModelMap modelMap){
        modelMap.addAttribute("p", p);
        return theme.getPcTemplate("group/list_group");
    }

    /**
     * 圈子详情页面
     *
     * @return
     */
    @GetMapping("/group/{groupId}")
    public String groupShow(@PathVariable("groupId") String groupId,
                            @RequestParam(value = "p", defaultValue = "1") int p
            , ModelMap modelMap){
        if (!StrUtils.checkLong(groupId)) {
            return forward("error/404");
        }
        Group content= groupService.findGroupById(Long.valueOf(groupId));
        if(content == null){
            return forward("error/404");
        }
        modelMap.addAttribute("groupId", groupId);
        modelMap.addAttribute("p", p);
        modelMap.addAttribute("content", BeanConvertor.convertBean(content, GroupDTO.class));
        return theme.getPcTemplate("group/detail");
    }
}
