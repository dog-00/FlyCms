package com.flycms.modules.group.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.group.domain.GroupTopicRecommend;
import com.flycms.modules.group.domain.dto.GroupTopicRecommendDTO;
import com.flycms.modules.group.service.IGroupTopicRecommendService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 内容推荐Controller
 * 
 * @author admin
 * @date 2020-12-07
 */
@RestController
@RequestMapping("/system/group/recommend")
public class GroupTopicRecommendAdminController extends BaseController
{
    @Autowired
    private IGroupTopicRecommendService groupTopicRecommendService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

/*    *//**
     * 新增内容推荐
     *//*
    @PreAuthorize("@ss.hasPermi('group:recommend:add')")
    @Log(title = "内容推荐", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GroupTopicRecommend groupTopicRecommend)
    {
        return toAjax(groupTopicRecommendService.insertGroupTopicRecommend(groupTopicRecommend));
    }*/

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除内容推荐
     */
    @PreAuthorize("@ss.hasPermi('group:recommend:remove')")
    @Log(title = "内容推荐", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(groupTopicRecommendService.deleteGroupTopicRecommendByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改内容推荐
     */
    @PreAuthorize("@ss.hasPermi('group:recommend:edit')")
    @Log(title = "内容推荐", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GroupTopicRecommend groupTopicRecommend)
    {
        return toAjax(groupTopicRecommendService.updateGroupTopicRecommend(groupTopicRecommend));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询内容推荐列表
     */
    @PreAuthorize("@ss.hasPermi('group:recommend:list')")
    @GetMapping("/list")
    public TableDataInfo list(GroupTopicRecommend groupTopicRecommend,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<GroupTopicRecommendDTO> pager = groupTopicRecommendService.selectGroupTopicRecommendPager(groupTopicRecommend, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出内容推荐列表
     */
    @PreAuthorize("@ss.hasPermi('group:recommend:export')")
    @Log(title = "内容推荐", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(GroupTopicRecommend groupTopicRecommend)
    {
        List<GroupTopicRecommendDTO> groupTopicRecommendList = groupTopicRecommendService.exportGroupTopicRecommendList(groupTopicRecommend);
        ExcelUtil<GroupTopicRecommendDTO> util = new ExcelUtil<GroupTopicRecommendDTO>(GroupTopicRecommendDTO.class);
        return util.exportExcel(groupTopicRecommendList, "recommend");
    }

    /**
     * 获取内容推荐详细信息
     */
    @PreAuthorize("@ss.hasPermi('group:recommend:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(groupTopicRecommendService.findGroupTopicRecommendById(id));
    }

}
