package com.flycms.modules.group.directive;

import com.flycms.framework.web.tag.BaseTag;
import com.flycms.modules.data.domain.Label;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.GroupTopicColumn;
import com.flycms.modules.group.domain.GroupUser;
import com.flycms.modules.group.service.IGroupService;
import com.flycms.modules.group.service.IGroupTopicColumnService;
import com.flycms.modules.group.service.IGroupUserService;
import com.flycms.modules.user.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class GroupDirective extends BaseTag {

    @Autowired
    private IGroupService groupService;;
    @Autowired
    private IGroupUserService groupUserService;
    @Autowired
    private IGroupTopicColumnService groupTopicColumnService;

    public GroupDirective() {
        super(GroupDirective.class.getName());
    }

    /**
     * 小组列表
     *
     * @param params
     * @return
     */
    public Object paginate(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        int status = getStatusParam(params,"status");
        String title = getParam(params, "title");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        Group group = new Group();
        group.setGroupName(title);
        System.out.println("----------"+status);
        group.setStatus(status);
        return groupService.selectGroupPager(group, pageNum, pageSize, sort, order);
    }

    /**
     * 话题分类列表
     *
     * @param params
     * @return
     */
    public Object column(Map params) {
        Long groupId = getLongParam(params, "groupId");
        GroupTopicColumn column = new GroupTopicColumn();
        column.setGroupId(groupId);
        return groupTopicColumnService.selectGroupTopicColumnList(column);
    }

    /**
     * 小组详细信息
     *
     * @param params
     * @return
     */
    public Object info(Map params) {
        Long id = getLongParam(params, "id");
        Group group = null;
        if(id != null){
            group = groupService.findGroupById(id);
        }
        return group;
    }

    /**
     * 用户关联小组列表
     *
     * @param params
     * @return
     */
    public Object group(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);

        Long userId = getLongParam(params, "userId");
        Long groupId = getLongParam(params, "groupId");
        Long isadmin = getLongParam(params, "isadmin");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");

        if(userId == null && groupId ==null){
            return null;
        }
        GroupUser groupUser = new GroupUser();
        groupUser.setUserId(userId);
        groupUser.setGroupId(groupId);
        groupUser.setIsadmin(isadmin);
        return groupUserService.selectGroupUserPager(groupUser, pageNum, pageSize, sort, order);
    }
}
