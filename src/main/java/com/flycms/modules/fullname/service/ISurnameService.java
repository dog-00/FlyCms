package com.flycms.modules.fullname.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.fullname.domain.Surname;
import com.flycms.modules.fullname.domain.dto.SurnameDTO;

/**
 * 姓氏Service接口
 * 
 * @author admin
 * @date 2020-10-13
 */
public interface ISurnameService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增姓氏
     *
     * @param surname 姓氏
     * @return 结果
     */
    public int insertSurname(Surname surname);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除姓氏
     *
     * @param ids 需要删除的姓氏ID
     * @return 结果
     */
    public int deleteSurnameByIds(Long[] ids);

    /**
     * 删除姓氏信息
     *
     * @param id 姓氏ID
     * @return 结果
     */
    public int deleteSurnameById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改姓氏
     *
     * @param surname 姓氏
     * @return 结果
     */
    public int updateSurname(Surname surname);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验姓氏是否唯一
     *
     * @param surname 姓氏
     * @return 结果
     */
    public String checkSurnameLastNameUnique(Surname surname);


    /**
     * 查询姓氏
     * 
     * @param id 姓氏ID
     * @return 姓氏
     */
    public SurnameDTO findSurnameById(Long id);

    /**
     * 查询姓氏列表
     * 
     * @param surname 姓氏
     * @return 姓氏集合
     */
    public Pager<SurnameDTO> selectSurnamePager(Surname surname, Integer page, Integer limit, String sort, String order);
}
