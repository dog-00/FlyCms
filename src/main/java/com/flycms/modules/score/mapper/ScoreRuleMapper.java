package com.flycms.modules.score.mapper;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.score.domain.ScoreRule;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


/**
 * 
 * 开发公司：97560.com<br/>
 * 版权：97560.com<br/>
 * <p>
 * 
 * 积分规则数据操作类
 * 
 * <p>
 * 
 * 区分　责任人　日期　　　　说明<br/>
 * 创建　孙开飞　2017年5月25日 　<br/>
 * <p>
 * *******
 * <p>
 * 
 * @author sun-kaifei
 * @email admin@97560.com
 * @version 1.0,2017年10月1日 <br/>
 * 
 */
@Repository
public interface ScoreRuleMapper {
	// ///////////////////////////////
	// /////     增加         ////////
	// ///////////////////////////////
	/** 
	 * 保存积分规则
	 * 
	 * @param scoreRule
	 * @return
	 */
	public int insertScoreRule(ScoreRule scoreRule);

	// ///////////////////////////////
	// /////        刪除      ////////
	// ///////////////////////////////
	/**
	 * 删除积分规则
	 *
	 * @param id 积分规则ID
	 * @return 结果
	 */
	public int deleteScoreRuleById(Long id);

	/**
	 * 批量删除积分规则
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	public int deleteScoreRuleByIds(Long[] ids);


	// ///////////////////////////////
	// /////        修改      ////////
	// ///////////////////////////////
	
	/**
	 * 修改积分规则
	 * 
	 * @param scoreRule
	 * @return
	 */
	public int updateScoreRule(ScoreRule scoreRule);

	/**
	 * 按id查询规则
	 *
	 * @param status 规则状态
	 * @param id 规则ID
	 * @return
	 */
	public int updateScoreRuleEnabled(Integer status,Long id);


	// ///////////////////////////////
	// /////        查詢      ////////
	// ///////////////////////////////
	/**
	 * 根据规则名称查找
	 * 
	 * @param name 规则名称
	 * @return
	 */
	public ScoreRule findScoreRuleByName(String name);
	
	/**
	 * 根据规则id查找
	 * 
	 * @param id
	 * @return
	 */
	public ScoreRule findScoreRuleById(Long id);

	//查询积分规则总数
	public int queryScoreRuleTotal(Pager pager);

	//查询积分规则列表
	public List<ScoreRule> selectScoreRulePager(Pager pager);

	/**
	 * 查询需要导出的积分规则列表
	 *
	 * @param scoreRule 积分规则
	 * @return 积分规则集合
	 */
	public List<ScoreRule> exportScoreRuleList(ScoreRule scoreRule);
}
