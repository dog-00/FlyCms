package com.flycms.modules.monitor.controller;

import java.util.List;

import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.framework.web.page.TableDataInfo;
import com.flycms.modules.monitor.service.IFlyOperLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.modules.monitor.domain.FlyOperLog;

/**
 * 操作日志记录
 * 
 * @author kaifei sun
 */
@RestController
@RequestMapping("/system/monitor/operlog")
public class FlyOperlogController extends BaseController
{
    @Autowired
    private IFlyOperLogService operLogService;

    @PreAuthorize("@ss.hasPermi('monitor:operlog:list')")
    @GetMapping("/list")
    public TableDataInfo list(FlyOperLog operLog)
    {
        startPage();
        List<FlyOperLog> list = operLogService.selectOperLogList(operLog);
        return getDataTable(list,10);
    }

    @Log(title = "操作日志", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('monitor:operlog:export')")
    @GetMapping("/export")
    public AjaxResult export(FlyOperLog operLog)
    {
        List<FlyOperLog> list = operLogService.selectOperLogList(operLog);
        ExcelUtil<FlyOperLog> util = new ExcelUtil<FlyOperLog>(FlyOperLog.class);
        return util.exportExcel(list, "操作日志");
    }

    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/{operIds}")
    public AjaxResult remove(@PathVariable Long[] operIds)
    {
        return toAjax(operLogService.deleteOperLogByIds(operIds));
    }

    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/clean")
    public AjaxResult clean()
    {
        operLogService.cleanOperLog();
        return AjaxResult.success();
    }
}
