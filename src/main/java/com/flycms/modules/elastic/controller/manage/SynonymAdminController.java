package com.flycms.modules.elastic.controller.manage;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.elastic.domain.Synonym;
import com.flycms.modules.elastic.domain.dto.SynonymDTO;
import com.flycms.modules.elastic.service.ISynonymService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

/**
 * 同义词词库Controller
 * 
 * @author admin
 * @date 2020-10-22
 */
@RestController
@RequestMapping("/system/elastic/synonym")
public class SynonymAdminController extends BaseController
{
    @Autowired
    private ISynonymService synonymService;

    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增同义词词库
     */
    @PreAuthorize("@ss.hasPermi('elastic:synonym:add')")
    @Log(title = "同义词词库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Synonym synonym)
    {
        if (UserConstants.NOT_UNIQUE.equals(synonymService.checkSynonymSynonymWordUnique(synonym)))
        {
            return AjaxResult.error("新增同义词词库'" + synonym.getSynonymWord() + "'失败，同义词已存在");
        }
        return toAjax(synonymService.insertSynonym(synonym));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除同义词词库
     */
    @PreAuthorize("@ss.hasPermi('elastic:synonym:remove')")
    @Log(title = "同义词词库", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(synonymService.deleteSynonymByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改同义词词库
     */
    @PreAuthorize("@ss.hasPermi('elastic:synonym:edit')")
    @Log(title = "同义词词库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Synonym synonym)
    {
        return toAjax(synonymService.updateSynonym(synonym));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询同义词词库列表
     */
    @PreAuthorize("@ss.hasPermi('elastic:synonym:list')")
    @GetMapping("/list")
    public TableDataInfo list(Synonym synonym,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SynonymDTO> pager = synonymService.selectSynonymPager(synonym, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出同义词词库列表
     */
    @PreAuthorize("@ss.hasPermi('elastic:synonym:export')")
    @Log(title = "同义词词库", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Synonym synonym,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @Sort @RequestParam(defaultValue = "id") String sort,
            @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SynonymDTO> pager = synonymService.selectSynonymPager(synonym, pageNum, pageSize, sort, order);
        ExcelUtil<SynonymDTO> util = new ExcelUtil<SynonymDTO>(SynonymDTO.class);
        return util.exportExcel(pager.getList(), "synonym");
    }

    /**
     * 获取同义词词库详细信息
     */
    @PreAuthorize("@ss.hasPermi('elastic:synonym:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(synonymService.findSynonymById(id));
    }

}
