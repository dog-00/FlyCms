$(document).ready(function(){
    $(document).on('click', '#comm-submit', function (){
        var title=$("#title").val(),columnId=$("#columnId").val(),content=editor.txt.html(),tag=$("#tag").val(),iscomment=$("#iscomment").val(),iscommentshow=$("#iscommentshow").val(),score=$("#score").val(),groupId=$("#groupId").val();
        $.ajax({
            url:'/user/my/topic/create',
            type:'post',
            data:{'title':title,'columnId':columnId,"content":content,"tag":tag,"iscomment":iscomment,"iscommentshow":iscommentshow,"score":score,"groupId":groupId},
            dataType:'json',
            success:function(data){
                if(data.code==200){
                    layer.msg("发布成功", { shift: -1 }, function () {
                        //location.href = "/user/my/topic/creat";
                    });
                }else{
                    alert(data.msg);
                }
            },
            error:function(){
                console.log('请求出错！');
            }
        })
    });

    $(document).on('click', '#update-submit', function (){
        var id=$("#id").val(),title=$("#title").val(),columnId=$("#columnId").val(),content=editor.txt.html(),tag=$("#tag").val(),iscomment=$("#iscomment").val(),iscommentshow=$("#iscommentshow").val(),score=$("#score").val();
        $.ajax({
            url:'/user/my/topic/update',
            type:'post',
            data:{'id':id,'title':title,'columnId':columnId,"content":content,"tag":tag,"iscomment":iscomment,"iscommentshow":iscommentshow,"score":score},
            dataType:'json',
            success:function(data){
                if(data.code==200){
                    layer.msg("更新成功", { shift: -1 }, function () {
                        location.href = "/topic/"+id;
                    });
                }else{
                    alert(data.msg);
                }
            },
            error:function(){
                console.log('请求出错！');
            }
        })
    });

    $(document).on('click', '#delete-topic', function (){
        var id = $(this).attr("data-id");
        layer.confirm('您是确定删除本条话题？！', {
            btn: ['确定','取消'] //按钮
        }, function(){
            $.ajax({
                url: "/user/my/topic/delete?"+Math.random(),
                data: {"id":id},
                dataType: "json",
                type :  "POST",
                cache : false,
                async: false,
                error : function(i, g, h) {
                    layer.msg('发送错误', {icon: 2});
                },
                success: function(ret){
                    if (ret.code >= 0) {
                        layer.msg("删除成功！", {icon: 1});
                        location.href = ret.data;
                        return false;
                    } else {
                        layer.msg(ret.message, {icon: 5});
                        return false;
                    }
                }
            });
        }, function(){
        });
    });
});